// entry point for asset compilation
require('./sass/site.scss');
var Blazy = require('blazy');
var ContentTiles = require('./js/content-tiles.js');
var Nav = require('./js/nav.js');
var prevButton = false;
var nextButton = false;

function keyPressed(event) {
  event = event || window.event;

  if (prevButton && event.keyCode === 37) {
    window.location.href = prevButton.href;
  } else if (nextButton && event.keyCode === 39) {
    window.location.href = nextButton.href;
  }
}

function start() {
  Nav.init();

  ContentTiles.init({
    innerClasses: ['image', 'image-overlay'],
  });

  setTimeout(function() {
    var bLazy = new Blazy({
      breakpoints: [{
        src: 'data-src-xs',
        width: 800,
      }],
      selector: '.image',
    });
  }, 500);

  nextButton = document.querySelector('.nav-arrow.next a');
  prevButton = document.querySelector('.nav-arrow.prev a');

  if (nextButton || prevButton) {
    document.onkeydown = keyPressed;
  }
};

function updateTiles() {
  setTimeout(function() {
    window.location.reload(true);
  }, 100);
};

window.onresize = updateTiles;
window.onorientationchange = updateTiles;

if (document.readyState !== 'loading') {
  start();
} else if (document.addEventListener) {
  document.addEventListener('DOMContentLoaded', start);
} else {
  document.attachEvent('onreadystatechange', function() {
    if (document.readyState === 'complete') {
      start();
    }
	});
}
