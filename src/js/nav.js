function hasClass(el, className) {
  return el.classList ? el.classList.contains(className) : new RegExp('\\b'+ className+'\\b').test(el.className);
}

function addClass(el, className) {
  if (el.classList) {
    el.classList.add(className);
  } else if (!hasClass(el, className)) {
    el.className += ' ' + className;
  }
}

function removeClass(el, className) {
  if (el.classList) {
    el.classList.remove(className);
  } else {
    el.className = el.className.replace(new RegExp('\\b'+ className+'\\b', 'g'), '');
  }
}

var hamburger = document.querySelector('.hamburger');
var mobileMenu = document.querySelector('.mobile-menu');

module.exports = {
  init: function() {
    hamburger.onclick = function () {
      if (hasClass(hamburger, 'is-active')) {
        removeClass(hamburger, 'is-active');
        removeClass(mobileMenu, 'is-active');
      } else {
        addClass(hamburger, 'is-active');
        addClass(mobileMenu, 'is-active');
      }
    };
  }
};
