const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin({
  filename: `./static/css/site.css`,
});

module.exports = {
  devtool: 'source-map',
  entry: {
    filename: './src/entry.js'
  },
  module: {
    rules: [{
      test: /\.scss$/,
      loader: extractSass.extract({
        use: [{
          loader: 'css-loader'
        }, {
          loader: 'sass-loader'
        }],
      })
    }, {
      test: /\.css$/,
      loader: 'style-loader!css-loader!postcss-loader',
      options: {
        plugins: function () {
          return [
            require('autoprefixer')
          ];
        }
      }
    }]
  },
  output: {
    filename: './static/js/site.js'
  },
  plugins: [
    extractSass
  ],
};
